import { shallowMount } from "@vue/test-utils";
import GMaps from "@/components/GMaps.vue";

// unit test for example 
describe("GMaps.vue", () => {
  it("renders props.building when passed", () => {
    const building = "";
    const wrapper = shallowMount(GMaps, {
      propsData: { building },
    });
    expect(wrapper.text()).toMatch(building);
  });
});
